# Deforestation Map of Borneo, Indonesia
This repository hosts an interactive map visualizing the deforestation probability and locations in Borneo, Indonesia. The map highlights areas with different probabilities of deforestation, providing a clear representation of regions at risk.

## How to Run Locally
Clone the repository:
git clone https://git.wur.nl/murk002/host-deforestation-map.git

### Navigate to the project directory:

cd your-repo-directory.

Open defor_map.html in your preferred browser.

## Alternatively, download defor_map.html and open in your preferred browser.